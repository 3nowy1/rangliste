<?php

/*
 * Copyright ATIW
 * Made with Love.
 */

namespace rangliste\controller;

/**
 * Controller Class of View/Matchhistory.
 * (Eventually) does the following things:
 *  - get Matches and MatchDetails from DataBase (X)
 *  - allow verification of Matches
 *  - allow deletion of Matches (X)
 *  - initiate recalculation of rankings
 *  - throw Exceptions on false Input for neat messages
 *
 * @author jakob.scherwat
 */
class Matchhistory {
    
    protected $database;
    protected $admin;
    
    function __construct($database) {
        $this->database = $database;
        $this->admin = isset($_SESSION['@dm1n']) ? $_SESSION['@dm1n'] : 0;
    }
    
    public function getAllMatches() {
        $query = 'SELECT * FROM matches';
        $query_res = mysqli_query($this->database->getLink(), $query);
        $res = [];
        while ($row = $query_res->fetch_row()) {
            $res[$row[0]]['challenger'] = $this->getFullName($row[1]);
            $res[$row[0]]['challenged'] = $this->getFullName($row[2]);
            $res[$row[0]]['date'] = $row[3];
            $res[$row[0]]['verified'] = !empty($row[4]);
            $res[$row[0]]['winner'] = $row[5];
            $res[$row[0]] += $this->getMatchDetails($row[0]);
        }
        return $res;
    }
    
    public function getFullName(int $id) {
        $query = 'SELECT vorname, nachname FROM mitglied WHERE mitgliedId = ' . $id;
        $query_res = mysqli_query($this->database->getLink(), $query);
        $res = $query_res->fetch_assoc();
        return utf8_encode($res['vorname']) . ' ' . utf8_encode($res['nachname']);
    }

    public function getMatchDetails(int $id) {
        $query = 'SELECT * FROM ergebnisse WHERE matchId = ' . $id;
        $query_res = mysqli_query($this->database->getLink(), $query);
        $res = [];
        while ($row = $query_res->fetch_row()) {
            $res['satz'.$row[1]]['challenger'] = $row[2];
            $res['satz'.$row[1]]['challenged'] = $row[3];
        }
        return $res;
    }
    
    public function isAdmin() {
        return $this->admin;
    }
    
    // Calls functions from corresponding checkbox names. Maybe too magical?
    // @throws Exception
    public function manageSelections(array $changes) {
        foreach ($changes as $name => $id) {
            $method = lcfirst(explode('_', $name)[0]);
            if (method_exists($this, $method)) {
                $this->$method($id);
            }
        }
        return true;
    }
    
    // Remove Matches + Details from DataBase.
    protected function deleteMatch(int $matchId) {
        $queryErg = 'DELETE FROM ergebnisse WHERE matchId = ' . $matchId;
        $query_resErg = mysqli_query($this->database->getLink(), $queryErg);
        $queryMatch = 'DELETE FROM matches WHERE matchId = ' . $matchId;
        $query_resMatch = mysqli_query($this->database->getLink(), $queryMatch);
        if (!($query_resMatch && $query_resErg)) {
            throw new Exception('Error: Match konnte nicht gelöscht werden.');
        }
    }
    
    // Set Match as valid in DataBase and renew ranking.
    protected function verificateMatch(int $matchId) {
        $query = 'SELECT * FROM matches WHERE matchId = ' . $matchId;
        $query_res = mysqli_query($this->database->getLink(), $query);
        $match_res = $query_res->fetch_assoc();
        if ($match_res['verifiziert']) {
            throw new Exception('Error: Match ist bereits verifiziert.');
        }
        if ($match_res['gewinner'] == $match_res['herausfordererId']) {
            try {
                $this->launchRankRecalc($match_res['herausfordererId'], $match_res['geforderterId']);
            $query = 'UPDATE matches SET verifiziert = 1 WHERE matchId = '.$matchId;
            $query_res = mysqli_query($this->database->getLink(), $query);
            } catch (Exception $ex) {
                throw $ex;
            }
        }
    }
    
    // Reorder Ranking
    protected function launchRankRecalc(int $winnerId, int $looserId) {
        $base_query = 'SELECT * FROM mitglied WHERE mitgliedId = ';
        $data_winner = mysqli_query($this->database->getLink(), $base_query.$winnerId)->fetch_row();
        $data_looser = mysqli_query($this->database->getLink(), $base_query.$looserId)->fetch_row();
        if ($data_winner[3] !== $data_looser[3]) {
            throw new Exception('Error: Matchteilnehmer sind nicht Teil derselben Rangliste.');
        }
        
        // challenger rank = challenged rank
        // zwischen challenged rank & challenger rank: rank+1
        $update_query = ' UPDATE mitglied SET rang = rang+1 WHERE ranglisteId = '.$data_winner[3]
                .' AND rang >= '.$data_looser[4].' AND rang < '.$data_winner[4];
        $update_query2 = 'UPDATE mitglied SET rang = '.$data_looser[4]
                .' WHERE mitgliedId = '.$data_winner[0];
        if (!(mysqli_query($this->database->getLink(), $update_query) && mysqli_query($this->database->getLink(), $update_query2))) {
            throw new Exception('Error: Ränge konnten nicht angepasst werden.');
        }
    }
}
