<?php

namespace rangliste\controller;
require '../model/eintragModel.php';
use rangliste\model\eintragModel;

/**
 * Controller class of view\Eintrag.
 * Used for verification and saving of correct matches.
 *
 * @author bartosz.michalak
 */
class eintragController {
    private $data;
    private $fehler = [];
    private $gewinner;
    private $model;
    
    function __construct($post) {
        foreach ($post as $key => $value) {
            $this->data[$key] = $value;
        }
        $this->model = new eintragModel($this->data);
    }
    
    public function getModel(){
        return $this->model;
    }
    
    public function getData(){
        return $this->normalizeData();
    }
    
    // Transforms user input for saving by eintragModel.
    private function normalizeData(){
        $data = $this->data;
        if(empty($data['herausforderer']) || empty($data['geforderter'])){
            $this->fehler[11] = 'Es müssen Spieler ausgewählt werden!';
        } else {
            if (!$this->checkChallangeAccepted()){
                $this->fehler[12] = "Es darf kein Mitglied mit einem niedriegerem Rang herausgefordert werden";
            }
            if(!$this->checkSpieler($data)){
                $this->fehler[10] = 'Die Spieler müssen der selben Rangliste zugeordnet sein!';
            }            
        }

        $data['satz'] = [1 => [
                            'herausfordererPunkte' => (int) $data['herausfordererFirst'],
                            'geforderterPunkte' => (int) $data['geforderterFirst']
                        ],
                        2 =>[
                            'herausfordererPunkte' => (int) $data['herausfordererSecond'],
                            'geforderterPunkte' => (int) $data['geforderterSecond']
                        ]];
        if(isset($data['herausfordererThird']) && isset($data['geforderterThird'])){
            $data['satz'][3] = [
                'herausfordererPunkte' => (int) $data['herausfordererThird'],
                'geforderterPunkte' => (int) $data['geforderterThird']
            ];
        }    
        $data['gewinner'] = $this->checkWinner($data['satz']);
        $data['fail'] = false;
        if($data['gewinner'] === null){
            $data['fail'] = true;
        }
        return $data;
    }
    
    // Check for correct user input & determines winner of match
    private function checkWinner($saetze){
        $herausfordererWon=0;
        $geforderterWon=0;
        foreach ($saetze as $satz => $punkte){
            if ($punkte['herausfordererPunkte'] == 30 && $punkte['geforderterPunkte'] == 30){
                $this->fehler[1] = 'Es dürfen nicht beide Spieler 30 Punkte in einem Satz gesammelt haben!';
            }
            if($punkte['herausfordererPunkte'] == 30 && $punkte['geforderterPunkte'] < 30){
                $herausfordererWon += 1;
            } else if($punkte['geforderterPunkte'] == 30 && $punkte['herausfordererPunkte'] < 30){
                $geforderterWon += 1;
            }
            if(($punkte['herausfordererPunkte'] != 21 && $punkte['geforderterPunkte'] != 21) && ($punkte['herausfordererPunkte'] != 30 && $punkte['geforderterPunkte'] != 30)){
                if(($punkte['herausfordererPunkte'] > 21 || $punkte['geforderterPunkte'] > 21)){
                    if((($punkte['herausfordererPunkte']-2) != $punkte['geforderterPunkte']) && (($punkte['geforderterPunkte']-2) != $punkte['herausfordererPunkte'])){
                        $this->fehler[2] = 'Punkte wurden falsch eingetragen!';
                    }    
                    if($punkte['herausfordererPunkte'] > 21 && $punkte['geforderterPunkte'] < $punkte['herausfordererPunkte']-1){
                        $herausfordererWon += 1;
                    } else if($punkte['geforderterPunkte'] > 21 && $punkte['herausfordererPunkte'] < $punkte['geforderterPunkte']-1){
                        $geforderterWon += 1;
                    }
                } else if($punkte['herausfordererPunkte'] < 21 && $punkte['geforderterPunkte'] < 21){
                    if($satz===3 && $punkte['herausfordererPunkte'] == 0 && $punkte['geforderterPunkte'] == 0){
                        continue;
                    } else {
                        $this->fehler[5] = 'Ein Satz endet, wenn ein Spieler mindestens 21 Punkte erreicht!';   
                    }
                }
            } else if($punkte['herausfordererPunkte'] == 21 && $punkte['geforderterPunkte'] < $punkte['herausfordererPunkte']-1){
                $herausfordererWon += 1;
            } else if($punkte['geforderterPunkte'] == 21 && $punkte['herausfordererPunkte'] < $punkte['geforderterPunkte']-1){
                $geforderterWon += 1;
            } else if($punkte['herausfordererPunkte'] == 21 && $punkte['geforderterPunkte'] > $punkte['herausfordererPunkte']+2){
                $this->fehler[3] = 'Punkte wurden falsch eingetragen!';
            } else if($punkte['geforderterPunkte'] == 21 && $punkte['herausfordererPunkte'] > $punkte['geforderterPunkte']+2){
                $this->fehler[4] = 'Punkte wurden falsch eingetragen!';
            }
            else if($punkte['geforderterPunkte'] >= 21 && $punkte['herausfordererPunkte'] == $punkte['geforderterPunkte']-1 && $punkte['geforderterPunkte'] !=30){
                $this->fehler[6] = 'Ein Satz endet, wenn ein Spieler mindestens 2 Punkte vorne liegt!';
            } else if($punkte['herausfordererPunkte'] >= 21 && $punkte['geforderterPunkte'] == $punkte['herausfordererPunkte']-1 && $punkte['herausfordererPunkte'] !=30){
                $this->fehler[7] = 'Ein Satz endet, wenn ein Spieler mindestens 2 Punkte vorne liegt!';
            }
            else if($punkte['geforderterPunkte'] == 30 && $punkte['herausfordererPunkte'] < $punkte['geforderterPunkte']-2){
                $this->fehler[8] = 'Punkte wurden falsch eingetragen!';
            } else if($punkte['herausfordererPunkte'] == 30 && $punkte['geforderterPunkte'] < $punkte['herausfordererPunkte']-2){
                    $this->fehler[9] = 'Punkte wurden falsch eingetragen!';
            } else if($punkte['herausfordererPunkte'] == $punkte['geforderterPunkte']){
                    $this->fehler[9] = 'Punkte wurden falsch eingetragen!';
            }
            // TODO 3 sätze wenn 2 gewonnnen fehler
            if(count($this->fehler) > 0){
                $this->gewinner=null;
                foreach ($this->fehler as $fail) {
                    echo '<div class="alert alert-danger" role="alert" style="text-align:center;">
                        '.$fail.'
                    </div>';
                }   
                unset($this->fehler);
                return null;
            }
        }
        if($herausfordererWon > $geforderterWon){
            if($herausfordererWon == 3) {
                echo '<div class="alert alert-danger" role="alert" style="text-align:center;">
                    Ein spieler kann maximal 2 Sätze gewinnen.
                </div>';
            } else {
                $this->gewinner = $this->data['herausforderer'];   
            }
        } else if($herausfordererWon < $geforderterWon){
            if($geforderterWon == 3) {
                echo '<div class="alert alert-danger" role="alert" style="text-align:center;">
                    Ein spieler kann maximal 2 Sätze gewinnen.
                    </div>';
            } else {
                $this->gewinner = $this->data['geforderter'];
            }
        } else {
            $this->gewinner=null;
            echo '<div class="alert alert-danger" role="alert" style="text-align:center;">
                Es konnte kein Gewinner ermittelt werden!
            </div>';
        }
        
        return $this->gewinner;
    }
    
    // Checks if players in match are not the same entity and in one ranking.
    private function checkSpieler(){
        $andereId = FALSE;
        $selbeRangliste = FALSE;
        if($this->data['herausforderer'] !== $this->data['geforderter']){
            $andereId = true;
        }
        if($this->model->getgeforderterRangliste() === $this->model->getHerausfordererRangliste()){
            $selbeRangliste = true;
        }
        return $andereId && $selbeRangliste;
    }
    
    // Give transformed data to model for saving.
    public function checkInsertSuccess($data){
        return $this->model->insertAllData($data);
    }
    
    // kann herausgefordert werden
    private function checkChallangeAccepted(){
        return ($this->model->getherausfordererRang() > $this->model->getgeforderterRang() && $this->model->getgeforderterRang() > $this->model->getherausfordererRang()-4);
    }
}