<?php
include 'includers/Header.php';
require_once '../controller/Matchhistory.php';

use rangliste\controller\Matchhistory;

$C = new Matchhistory($db);

$msg = null;
if (!empty($_POST)){
    try {
        $success = $C->manageSelections($_POST);
        $msg = $success ? 'Änderungen erfolgreich.' : null;
    } catch (Exception $x) {
        $msg = $x->getMessage();
    }
}

$allEntries = $C->getAllMatches();

 if (!empty($msg)): ?>
<div class="text-center alert alert-<?= (strrpos($msg, 'Error')) ? 'warning' : 'success' ?>" role="alert">
<?= $msg ?>
</div>
<?php endif; ?>
<h2 class="text-center">Matchhistorie</h2>

<form method="post">
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Matchup</th>
                    <th>Satz 1</th>
                    <th>Satz 2</th>
                    <th>Satz 3</th>
                    <th>Datum</th>
                    <?php if ($C->isAdmin()): ?>
                    <th>Verifiziert</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allEntries as $id => $entry): ?>
                <tr>
                    <td>
                        <?php echo $entry['challenger'].' vs '.$entry['challenged'] ?>
                    </td>
                    <td>
                        <?php if (isset($entry['satz1'])):
                            echo $entry['satz1']['challenger'].' : '.$entry['satz1']['challenged'];
                        endif; ?>
                    </td>
                    <td>
                        <?php if (isset($entry['satz2'])):
                            echo $entry['satz2']['challenger'].' : '.$entry['satz2']['challenged'];
                        endif; ?>
                    </td>
                    <td>
                        <?php if (isset($entry['satz3'])):
                            echo $entry['satz3']['challenger'].' : '.$entry['satz3']['challenged'];
                        endif; ?>
                    </td>
                    <td>
                        <?= $entry['date'] ?>
                    </td>
                    <?php if ($C->isAdmin()): ?>
                    <td>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" value="<?php echo $id ?>" name="<?php echo 'VerificateMatch_'.$id ?>" <?php echo (empty($entry['verified']) ?: 'disabled checked') ?>>
                            <label class="form-check-label">Verifizieren</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" value="<?php echo $id ?>" name="<?php echo 'DeleteMatch_'.$id ?>">
                            <label class="form-check-label">Löschen</label>
                        </div>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php if ($C->isAdmin()): ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto" style="margin: 2em">
                <input class="btn btn-primary" type="submit" value="Änderungen Übernehmen">
            </div>
        </div>
    </div>
    <?php endif; ?>
</form>
<div class="container">
    <div class="row text-center">
        <div class="col-12 white">Einträge 1 - <?php echo count($allEntries) ?></div>
    </div>
</div>

<?php
include 'includers/Footer.php';