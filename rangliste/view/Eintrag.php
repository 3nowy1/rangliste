<?php
include 'includers/Header.php';
require '../controller/eintragController.php';

use rangliste\controller\eintragController;
$controller = new eintragController($_POST);
$model = $controller->getModel();
$mitgliederListe = $model->getMitglieder();

?>

<div class="container">
    <form method="post" action="Eintrag.php">
        <div class="row text-center">
            <div class="col-md-5">
                <h3>Herausforderer</h3>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <h3>Geforderter</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-5">
                <select class="form-control" id="NewMatchChallenger" name="herausforderer">
                    <option value="">Vorname Name</option>
                    <?php 
                        foreach (array_keys($mitgliederListe) as $key){
                            echo '<option value="'.$key.'">'.$mitgliederListe[$key]['vorname'].' '.$mitgliederListe[$key]['nachname'].'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-2">
                <h3>VS</h3>
            </div>
            <div class="col-md-5">
                <select class="form-control" id="NewMatchChallenged" name="geforderter">
                    <option value="">Vorname Name</option>
                    <?php 
                        foreach (array_keys($mitgliederListe) as $key){
                            echo '<option value="'.$key.'">'.$mitgliederListe[$key]['vorname'].' '.$mitgliederListe[$key]['nachname'].'</option>';
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="offset-md-5 col-md-2">
                <input class="form-control text-center" name="matchDate" id="NewMatchDateTime" type="text" placeholder="<?php echo date("d.m.Y H:i", time()) ?>" disabled>
            </div>
        </div>
        <div class="row" style="margin-top: 1.75em">
            <div class="offset-md-3 col-md-2">
                <input class="form-control" id="NewMatchSet1Challenger" min="0" max="30" value="0" type="number" name="herausfordererFirst" required>
            </div>
            <div class="col-md-2 text-center">
                <h4>Satz 1</h4>
            </div>
            <div class="col-md-2">
                <input class="form-control" id="NewMatchSet1Challenged" min="0" max="30" value="0" type="number" name="geforderterFirst" required>
            </div>
        </div>
        <div class="row" style="margin-top: 1.75em">
            <div class="offset-md-3 col-md-2">
                <input class="form-control" id="NewMatchSet2Challenger" min="0" max="30" value="0" type="number" name="herausfordererSecond" required>
            </div>
            <div class="col-md-2 text-center">
                <h4>Satz 2</h4>
            </div>
            <div class="col-md-2">
                <input class="form-control" id="NewMatchSet2Challenged" min="0" max="30" value="0" type="number" name="geforderterSecond" required>
            </div>
        </div>
        <div class="row" style="margin-top: 1.75em">
            <div class="offset-md-3 col-md-2">
                <input class="form-control" id="NewMatchSet3Challenger" min="0" max="30" value="0" type="number" name="herausfordererThird" required>
            </div>
            <div class="col-md-2 text-center">
                <h4>Satz 3</h4>
            </div>
            <div class="col-md-2">
                <input class="form-control" id="NewMatchSet3Challenged" min="0" max="30" value="0" type="number" name="geforderterThird" required>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-auto" style="margin: 2em">
                <input class="btn btn-primary" name="speichernBtn" type="submit" value="Speichern">
            </div>
        </div>
    </form>
</div>



<?php
if(isset($_POST['speichernBtn'])){
    $data = $controller->getData();
    
    if(true !== $data['fail']){
        if($controller->checkInsertSuccess($data)){
            echo '<div class="alert alert-success" role="alert" style="text-align:center;">
                Die Ergebnisse wurden erfolgreich gespeichert!
            </div>';
        }   
    }
}
include 'includers/Footer.php';