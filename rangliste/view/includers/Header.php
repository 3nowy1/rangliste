<html>
    <head>
        <!--bootstrap css-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <!-- bootstrap js -->
       <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
       
        <style>
            .nav-item a:hover {
                color: #f2f8ff !important;
                cursor: pointer;
            }
        </style>
    </head>
    <body style="background-color: #f2f8ff;">
        <nav class="border border-dark navbar navbar-light navbar-expand-md" style="background-color: #93c5ff;">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon left"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                  <a class="nav-link" href="rangliste.php" style="color:#444; font-weight: bold;">Rangliste</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="Eintrag.php" style="color:#444; font-weight: bold;">Neuer Eintrag</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Matchhistory.php" style="color:#444; font-weight: bold;">Matchhistorie</a>
              </li>
            </ul>
          </div>
        </nav>
        <div style="margin: 4em"> 
            <?php
                require '../config/dbConnection.php';
                use rangliste\config\dbConnection;

                $db = new dbConnection();
                
                //me me big admin
                session_start();
                if(isset($_GET['@dm1n']) && $_GET['@dm1n'] == 1){
                    $_SESSION['@dm1n'] = 1;
                }
            ?>
