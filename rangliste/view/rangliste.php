<?php
include('./includers/Header.php');

require '../model/ranglisteModel.php';
use rangliste\model\ranglisteModel;

$selectedRanglisteId = isset($_COOKIE['ranglisteId']) ? $_COOKIE['ranglisteId'] : 0;
$model = new ranglisteModel();
$rangliste = $model->getRanglisten();
$mitgliederListe = $model->getMitgliederListe();

?>
<script type="text/javascript" src="../js/rangliste.js"></script>
<center><h1>Rangliste</h1></center>
<br>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-8">
            </div>
            <div class="col-md-4" style="padding-bottom:10px;">
                <select class="form-control" id="rangliste" onchange="toggleRangliste(this);">
                    <?php 
                        echo '<option value="'.$selectedRanglisteId.'">'.$rangliste[$selectedRanglisteId].'</option>';
                        unset($rangliste[$selectedRanglisteId]);
                        foreach ($rangliste as $id => $liste) {
                            echo '<option value="'.$id.'">'.$liste.'</option>';
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Rang</th>
      <th scope="col">Vorname</th>
      <th scope="col">Nachname</th>
    </tr>
  </thead>
  <tbody>
      <?php
        foreach (array_keys($mitgliederListe) as $key){
            echo '<tr>';
                    echo '<td>'.$mitgliederListe[$key]['rang'].'</td>';
                    echo '<td>'.$mitgliederListe[$key]['vorname'].'</td>';
                    echo '<td>'.$mitgliederListe[$key]['nachname'].'</td>';
            echo '</tr>';
        }
      ?>
  </tbody>
</table>


<?php
include ('./includers/Footer.php');
?>