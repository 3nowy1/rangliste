<?php

namespace rangliste\model;

use rangliste\config\dbConnection;
use rangliste\controller\eintragController;

/**
 * Work with database to save and read matches.
 *
 * @author bartosz.michalak
 */
class eintragModel {
    
    private $link;
    
    function __construct($data) {
        $db = new dbConnection();
        $this->data=$data;
        $this->link = $db->getLink(); 
    }
    
    // Read all members from all rankings, sorted by vorname.
    public function getMitglieder(){
        $result=mysqli_query($this->link, 'SELECT vorname, nachname, mitgliedId from `mitglied` ORDER BY `vorname`, `nachname`, `mitgliedId` ASC');
        $mitgliederListe = [];
        $mitgliederCount=0;
        while ($row = $result->fetch_row()) {
            $mitgliederListe[$row[2]]['vorname'] = utf8_encode($row[0]);
            $mitgliederListe[$row[2]]['nachname'] = utf8_encode($row[1]);
            $mitgliederCount++;
        }
        return $mitgliederListe;
    } 
    
    // Read the last given member id.
    public function getAutoIncrementMitgliedId(){
        $matchIdQuery = mysqli_query($this->link, 'SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = "matches"');
        return mysqli_fetch_row($matchIdQuery)[0];
    }
    
    // Inserts match and match details into data base.
    public function insertAllData($data){
        $id = $this->getAutoIncrementMitgliedId();
        $match = $this->insertMatch($data, $id);
        if($match){
            $erg = $this->insertErgebnisse($data['satz'], $id);   
        }
        return ($match && $erg);
    }
    
    public function insertMatch($data, $id){
        $matchesInsert = mysqli_real_escape_string($this->link, 'INSERT INTO matches (matchId, herausfordererId, geforderterId, verifiziert, gewinner) VALUES ('.$id.','.$this->data['herausforderer'].','.$this->data['geforderter'].',0,'.$data['gewinner'].')');
        return mysqli_query($this->link, $matchesInsert);
    }

    public function insertErgebnisse($data, $id){
        foreach ($data as $satz => $punkte) {
            $ergebnisseInsert = mysqli_real_escape_string($this->link, 'INSERT INTO ergebnisse (matchId, satz, herausfordererPunkte, geforderterPunkte) VALUES ('.$id.','.$satz.','.$punkte['herausfordererPunkte'].','.$punkte['geforderterPunkte'].')');
            $ergebnissExec = mysqli_query($this->link, $ergebnisseInsert);
        }
        return $ergebnissExec;
    }
    
    public function getHerausfordererRangliste(){
        $herausfordererQuery = mysqli_query($this->link, 'SELECT ranglisteId FROM mitglied WHERE mitgliedId ='.$this->data['herausforderer']);
        return mysqli_fetch_row($herausfordererQuery)[0];
    }
    
    public function getgeforderterRangliste(){
        $geforderterQuery = mysqli_query($this->link, 'SELECT ranglisteId FROM mitglied WHERE mitgliedId ='.$this->data['geforderter']);
        return mysqli_fetch_row($geforderterQuery)[0];
    }
    
    public function getherausfordererRang(){
        $herausfordererRang = mysqli_query($this->link, 'SELECT rang FROM mitglied WHERE mitgliedId ='.$this->data['herausforderer']);
        return mysqli_fetch_row($herausfordererRang)[0];
    }

    public function getgeforderterRang(){
        $geforderterRang = mysqli_query($this->link, 'SELECT rang FROM mitglied WHERE mitgliedId ='.$this->data['geforderter']);
        return mysqli_fetch_row($geforderterRang)[0];
    }

}
