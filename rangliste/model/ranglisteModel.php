<?php

namespace rangliste\model;

use rangliste\config\dbConnection;
/**
 * Work with database to read ranking data.
 *
 * @author bartosz.michalak
 */
class ranglisteModel {
       
    private $link;
    
    function __construct() {
        $db = new dbConnection();
        $this->link = $db->getLink(); 
    }
    
    public function getRanglisten(){
        $result=mysqli_query($this->link, 'SELECT * FROM rangliste');
        $rangliste = [];
        while ($row = $result->fetch_row()) {
            $rangliste[$row[0]] = utf8_encode($row[1]);
        }
        
        return $rangliste;
    }
    
    public function getMitgliederListe(){
        $resultMitglieder=mysqli_query($this->link, 'SELECT vorname, nachname, rang from `mitglied` WHERE `ranglisteId`='.(isset($_COOKIE['ranglisteId']) ? $_COOKIE['ranglisteId'] : 0).' ORDER BY `rang` ASC');
        $mitgliederListe = [];
        $mitgliederCount=0;
        while ($row = $resultMitglieder->fetch_row()) {
            $mitgliederListe[$mitgliederCount]['vorname'] = utf8_encode($row[0]);
            $mitgliederListe[$mitgliederCount]['nachname'] = utf8_encode($row[1]);
            $mitgliederListe[$mitgliederCount]['rang'] = $row[2];
            $mitgliederCount++;
        }
        
        return $mitgliederListe;
    }
}
