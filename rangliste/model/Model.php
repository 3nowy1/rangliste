<?php
require '../config/dbConnection.php';
use rangliste\config\dbConnection;
use rangliste\model\eintragModel;

namespace rangliste\model;

/**
 * This class includes methods for models.
 *
 * @author bartosz.michalak
 * @abstract
 */
abstract class Model{
    /**
     * object of the class dbConnection
     *
     * @var object
     */
    protected $link;
 
    /**
     * It sets connect with the database.
     *
     * @return void
     */
    public function  __construct() {
        $db = new dbConnection();
        $this->link = $db->link; 
    }
    /**
     * It loads the object with the model.
     *
     * @param string $name name class with the class
     * @param string $path pathway to the file with the class
     *
     * @return object
     */
    public function loadModel($name, $path='') { 
        switch ($name){
            case 'eintrag':
                $model = new eintragModel();
                break;
            default: return false;
        }
    }
}