<?php

namespace rangliste\config;

/**
 * Die Klasse bereitet eine Verbindung mit der Datenbank vor.
 *
 * @author bartosz.michalak
 */
class dbConnection {
    protected $link;
    
    public function __construct() {
        $host = "127.0.0.1";
        $user = "root";
        $password = "";
        $database = "usr_web170_7";
        $this->link = mysqli_connect($host, $user, $password, $database);

        if(mysqli_connect_errno()) {
                echo "Connection Failed: " . mysqli_connect_errno();
                exit();
        }

    }
    
    public function close(){
        mysqli_close($this->link);
    }

    public function getLink() {
        return $this->link;
    }
}
