-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 27. Sep 2018 um 10:43
-- Server-Version: 10.1.35-MariaDB
-- PHP-Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `usr_web170_7`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ergebnisse`
--

CREATE TABLE `ergebnisse` (
  `matchId` int(11) NOT NULL,
  `satz` int(11) NOT NULL,
  `herausfordererPunkte` int(11) NOT NULL,
  `geforderterPunkte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `matches`
--

CREATE TABLE `matches` (
  `matchId` int(11) NOT NULL,
  `herausfordererId` int(11) NOT NULL,
  `geforderterId` int(11) NOT NULL,
  `datum` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `verifiziert` enum('0','1') COLLATE latin1_german1_ci NOT NULL,
  `gewinner` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mitglied`
--

CREATE TABLE `mitglied` (
  `mitgliedId` int(11) NOT NULL,
  `vorname` varchar(30) COLLATE latin1_german1_ci NOT NULL,
  `nachname` varchar(30) COLLATE latin1_german1_ci NOT NULL,
  `ranglisteId` int(11) DEFAULT NULL,
  `rang` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `mitglied`
--

INSERT INTO `mitglied` (`mitgliedId`, `vorname`, `nachname`, `ranglisteId`, `rang`) VALUES
(1, 'Karsten', 'Kraatz', 0, 1),
(2, 'Kevin', 'Kraatz', 0, 2),
(3, 'Marvin', 'Schäfers', 0, 3),
(4, 'Tim', 'Brüggenthies', 0, 4),
(5, 'Tim', 'Zimbelmann', 0, 5),
(6, 'Marius', 'Schneider', 0, 6),
(7, 'Dirk', 'Stichling', 0, 7),
(8, 'Bohdan', 'Kurylak', 0, 8),
(9, 'Frederik', 'Willeke', 0, 9),
(10, 'Jonas', 'Herzel', 0, 10),
(11, 'Jan', 'Stichling', 0, 11),
(12, 'Tim', 'Hecker', 0, 12),
(13, 'Lennon', 'Apelt', 0, 13),
(14, 'Pascal', 'Schulte', 0, 14),
(15, 'Tobias', 'Baena-Perez', 0, 15),
(16, 'Felix', 'Mühlenhoff', 0, 16),
(17, 'Niklas', 'Sander', 0, 17),
(18, 'Johannes', 'Kräußel', 0, 18),
(19, 'Wolfgang', 'Mügge', 0, 19),
(20, 'Christine', 'Marzi', 1, 1),
(21, 'Elise', 'Schmidt', 1, 2),
(22, 'Lena', 'Morgenstern', 1, 3),
(23, 'Kimberly', 'Knust', 1, 4),
(24, 'Pia', 'Kraatz', 1, 5),
(25, 'Jana', 'Pilz', 1, 6),
(26, 'Karin', 'Geck-Mügge', 1, 7),
(27, 'Hanne', 'Hecker', 1, 8),
(28, 'Lena', 'Dörenkamp', 1, 9),
(29, 'Tanja', 'Graupner', 1, 10),
(30, 'Christiane', 'Meierfrankenfeld', 1, 11),
(31, 'Sinika', 'Apelt', 1, 12),
(32, 'Lina', 'Meierfrankenfeld', 2, 1),
(33, 'Mats', 'Kempe', 2, 2),
(34, 'Aliena', 'Apelt', 2, 3),
(35, 'Dania', 'Karnath', 2, 4),
(36, 'Frederick', 'Giebelhausen', 2, 5),
(37, 'Konstantin', 'Kleine', 2, 6),
(38, 'Amy', 'Billig', 2, 7),
(39, 'Victoria', 'Trost', 2, 8),
(40, 'Merten', 'Oelmann', 2, 9),
(41, 'Thilo', 'Neumann', 2, 10),
(42, 'Mael', 'Nguidjol', 2, 11),
(43, 'Leonardo', 'Apel', 2, 12),
(44, 'Johannes', 'Mühlenhoff', 2, 13),
(45, 'Nick', 'Schamne', 2, 14),
(46, 'Philip', 'Priemer', 2, 15);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rangliste`
--

CREATE TABLE `rangliste` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `rangliste`
--

INSERT INTO `rangliste` (`id`, `name`) VALUES
(0, 'Rangliste Herren U15 - Senioren'),
(1, 'Rangliste Damen U15 - Senioren'),
(2, 'Rangliste U13 und jünger');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `ergebnisse`
--
ALTER TABLE `ergebnisse`
  ADD PRIMARY KEY (`matchId`,`satz`);

--
-- Indizes für die Tabelle `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`matchId`),
  ADD KEY `herausfordererId` (`herausfordererId`),
  ADD KEY `geforderterId` (`geforderterId`),
  ADD KEY `gewinner` (`gewinner`);

--
-- Indizes für die Tabelle `mitglied`
--
ALTER TABLE `mitglied`
  ADD PRIMARY KEY (`mitgliedId`),
  ADD KEY `ranglisteId` (`ranglisteId`);

--
-- Indizes für die Tabelle `rangliste`
--
ALTER TABLE `rangliste`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `matches`
--
ALTER TABLE `matches`
  MODIFY `matchId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `mitglied`
--
ALTER TABLE `mitglied`
  MODIFY `mitgliedId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `ergebnisse`
--
ALTER TABLE `ergebnisse`
  ADD CONSTRAINT `ergebnisse_ibfk_1` FOREIGN KEY (`matchId`) REFERENCES `matches` (`matchId`);

--
-- Constraints der Tabelle `matches`
--
ALTER TABLE `matches`
  ADD CONSTRAINT `matches_ibfk_1` FOREIGN KEY (`herausfordererId`) REFERENCES `mitglied` (`mitgliedId`),
  ADD CONSTRAINT `matches_ibfk_2` FOREIGN KEY (`geforderterId`) REFERENCES `mitglied` (`mitgliedId`),
  ADD CONSTRAINT `matches_ibfk_3` FOREIGN KEY (`gewinner`) REFERENCES `mitglied` (`mitgliedId`);

--
-- Constraints der Tabelle `mitglied`
--
ALTER TABLE `mitglied`
  ADD CONSTRAINT `mitglied_ibfk_1` FOREIGN KEY (`ranglisteId`) REFERENCES `rangliste` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
